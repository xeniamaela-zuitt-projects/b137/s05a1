package b137.abano.s05a1;

import java.util.ArrayList;

public class Phonebook{

    private ArrayList<Contact> contacts = new ArrayList<>();

    //empty constructor
    public Phonebook() {}

    //parameterized constructor
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    //Getters & Setters
    public ArrayList<Contact> getPhonebook () {
        return contacts;
    }

    public void setPhonebook(ArrayList<Contact>phonebook) {
        this.contacts = phonebook;
    }

    //add contact to ArrayList
    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    //method
    public void phonebookEmpty() {
        if (contacts.size() < 1) {
            System.out.println("Phonebook Empty.");
        } else {
            for (Contact contact: contacts) {
            System.out.println(contact.name);
            System.out.println("-------------------");
            System.out.println(contact.name + " has the following registered numbers:");
            contact.showNumbers();
            System.out.println("----------------------------------");
            System.out.println(contact.name + " has the following registered addresses:");
            contact.showAddresses();
            System.out.println("==================================");
            }
        }
    }
}
