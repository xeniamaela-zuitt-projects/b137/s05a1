package b137.abano.s05a1;

import java.util.ArrayList;

public class Contact {
    String name;
    ArrayList<String> numbers = new ArrayList<>();
    ArrayList<String> addresses = new ArrayList<>();

    public Contact() {}

    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses){
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    //getter
    public String getName () {
        return name;
    }

    public ArrayList<String> getNumbers () {
        return numbers;
    }

    public ArrayList<String> getAddresses () {
        return addresses;
    }

    // Setter
    public void setName(String newName) {
        this.name = newName;
    }

    public void setNumbers(ArrayList<String> newNumbers) {
        this.numbers = newNumbers;
    }

    public void setAddresses(ArrayList<String> newAddresses) {
        this.addresses = newAddresses;
    }

    //add
    public void addNumber(String numbers) {
        this.numbers.add(numbers);
    }

    public void addAddresses(String addresses) {
        this.numbers.add(addresses);
    }


    public void showNumbers() {

        for (String numbers : this.numbers) {
            System.out.println(numbers);
        }

    }

    public void showAddresses() {
        for (String addresses : this.addresses) System.out.println(addresses);
    }


}
