package b137.abano.s05a1;

public class Main {
    public static void main(String[] args){

        Phonebook pb = new Phonebook();

        Contact contactA = new Contact();
        Contact contactB = new Contact();

        contactA.setName("John Doe");
        contactB.setName("Jane Doe");

        contactA.addNumber("+639152468596");
        contactA.addNumber("+639228547963");
        contactB.addNumber("+639162148573");
        contactB.addNumber("+639173698541");

        contactA.addAddresses("my home in Quezon City");
        contactA.addAddresses("my office in Makati City");
        contactB.addAddresses("my home in Caloocan City");
        contactB.addAddresses("my office in Pasay City");

        pb.addContact(contactA);
        pb.addContact(contactB);

        pb.phonebookEmpty();
    }
}
